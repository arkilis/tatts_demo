//
//  CompanyDataFetch.swift
//  Tatts_Test
//
//  Created by Ben on 11/9/17.
//  Copyright © 2017 Yang. All rights reserved.
//

import UIKit


class CompanyTableViewModel: NSObject{
    
    var aryResults = [CompanyModel]()
    var dataManager = DataManager()
    
    
    func getData(completion: @escaping () -> Void) {
        dataManager.getAllItems(completion:{ (result: Array<CompanyModel>) in
            for item in result{
                self.aryResults.append(item)
            }
            completion()
        });
    }
 
    
    /** section */
    func numberOfSections() -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    /** sections */
    func numberOfRowsInSection() -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.aryResults.count + 1
    }
    
    /** height */
    func heightForRowAt(heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row==0){
            return 44
        }
        else{
            return 80
        }
    }
    
    /** cell */
    func cellForRowAt(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row==0){
            let cell = UITableViewCell()
            cell.textLabel?.text = "Please select your lottery provider"
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textLabel?.font = UIFont(name:"HelveticaNeue-Bold", size: 14)
            cell.textLabel?.textAlignment = .center
            return cell
        }else{
            let cell = TattsCompanyTableCell.init(style: .default, reuseIdentifier: "companyCell")
            if let mCompany = aryResults[indexPath.row-1] as? CompanyModel {
                cell.createLayout(szLogoURL: mCompany.companyLogoURL, szCompanyDesc: mCompany.companyDescription)
            }
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }
}
