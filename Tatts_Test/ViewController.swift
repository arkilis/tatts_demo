//
//  ViewController.swift
//  Tatts_Test
//
//  Created by Ben on 11/9/17.
//  Copyright © 2017 Yang. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    //let GET_ITEMS_URL = "https://api.tatts.com/svc/sales/vmax/web/data/lotto/companies"
    
    var viewModel = CompanyTableViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // bar button item
        let redoBarButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(onRedo))
        navigationItem.rightBarButtonItems = [redoBarButton]
        
        self.tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        
        viewModel.getData {
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // custom functions
    func onRedo(){
        
    }
    
    // Table override
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return self.viewModel.heightForRowAt(heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.viewModel.cellForRowAt(tableView, cellForRowAt:indexPath)
    }
}

