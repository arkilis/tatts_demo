//
//  CompanyModel.swift
//  Tatts_Test
//
//  Created by Yang on 11/9/17.
//  Copyright © 2017 Yang. All rights reserved.
//

import Foundation
import UIKit

class CompanyModel: NSObject {
    
    let KEY_COMPANY_ID          = "COMPANY_ID"
    let KEY_COMPANY_DISPLAYNAME = "DISPLAY_NAME"
    let KEY_COMPANY_DESCRIPTION = "DESCRIPTION"
    let KEY_COMPANY_LOGOURL     = "LOGO_URL"
    
    var companyId: String!
    var companyDisplayName: String!
    var companyDescription: String!
    var companyLogoURL: String!
}
