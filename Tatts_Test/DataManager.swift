//
//  DataManager.swift
//  Tatts_Test
//
//  Created by Ben on 11/9/17.
//  Copyright © 2017 Yang. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON


class DataManager: NSObject {
    
    let GET_ITEMS_URL = "https://api.tatts.com/svc/sales/vmax/web/data/lotto/companies"
    
    let httpHeaders: HTTPHeaders = [
        "Content-Type": "application/json"
    ]
    
    // get latest most hot 10 deals
    func getAllItems(completion: @escaping (_  result: Array<CompanyModel>) -> Void) -> () {
        
        var aryItems = [CompanyModel]()
        Alamofire.request(GET_ITEMS_URL, method: HTTPMethod.get, encoding:URLEncoding.default, headers: httpHeaders).responseJSON { response in
            
            if let JSON = response.result.value as? [String:AnyObject] {
                
                //print("JSON: \(JSON["objects"])")
                for jsonItem in JSON["Companies"] as! [Dictionary<String, AnyObject>]{
                        let item = CompanyModel()
                        item.companyId = jsonItem["CompanyId"] as? String
                        item.companyDisplayName = jsonItem["CompanyDisplayName"] as? String
                        item.companyDescription = jsonItem["CompanyDescription"] as? String
                        item.companyLogoURL = jsonItem["CompanyLogoUrl"] as? String
                        aryItems.append(item)
                }
            }
            completion(aryItems)
        }
    }
}
